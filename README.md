# orbit-db-migrator

Usage: ./orbitmigrator.js [OPTIONS]... [DB URL]...  [ORBIT ROOT URL]... [FILE]...

Description:
It import or exports database data and schemas between the file system and a given OrbitDb instance. Each desired operation is identified by a given option followed its needed parameters. It can process data and schema dumps and imports in batches.

Options:
```
    --dump-data     It dumps all the data of the database given by [DB URL] to a [FILE].        
    --dump-schema   It dump the schema of the database given by [DB URL] to a [FILE].    
    --import-data   It imports the data contained in a given dump [FILE] with all the elements to the destination [DB URL].
    --import-schema It imports a given schema in [FILE] with the database definition to a remote database [DB URL].
    --dump-all      It dumps all the existing databases in a given instance [ORBIT ROOT URL] to a given folder [FOLDER].
    --import-all    It imports all the existing data (please note that the files must respect the same format of the --dump-all option) into the OrbitDb instance given by [ORBIT ROOT URL].
```

Parameters:
```
    [FILE] A given file existing in the file system containing data or a schema.
    [FOLDER] A given folder existing in the file system containing data or schema files, previously dumped.
    [DB URL] The OrbitDb URL for a given db instance. Please note that each OrbitDb database has a different URL.
    [ORBIT ROOT URL] The OrbitDb root URL. This will be the root for the OrbitDb instance, without specifying a given database.
```