#!/usr/bin/env node
/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

const http = require('http');
const fs = require('fs');
const StreamArray = require('stream-json/streamers/StreamArray');

const MAX_CONCURRENT_CONNECTIONS = 50; // maximum number of connections to the server.

//OrbitDB special paths.
const ORBITDB_GET_ALL_PATH = "/all";
const ORBITDB_GET_SCHEMA_PATH = "";
const ORBITDB_CREATE_DOC_PATH = "/put";
const ORBITDB_GET_DBS = "/dbs";

//cmd options
const OPTION_CMD_HELP = "--help";
const OPTION_CMD_DUMP_DATA = "--dump-data";
const OPTION_CMD_DUMP_SCHEMA = "--dump-schema";
const OPTION_CMD_IMPORT_DATA = "--import-data";
const OPTION_CMD_IMPORT_SCHEMA = "--import-schema";
const OPTION_CMD_DUMP_ALL = "--dump-all";
const OPTION_CMD_IMPORT_ALL = "--import-all";

const SCHEMA_FILE_SUFFIX = "_schema.json";
const DATA_FILE_SUFFIX = "_data.json";

/**
 * It handles OrbitDB data migrations.
 * It allows to dump and import data between different OrbitDb instances using an extremely memory efficient JSON handling.
 * @todo Change this to use the Promises and async/await functions. Right now it dumps all asyncronously but it will be better to dump one database at a time. 
 */
class OrbitMigrator {

    /**
     * Taking a folder it will run the import for all the spects and data files found.
     * @param {*} folder The folder containing the dump files.
     * @param {*} orbitDbUrl The destination database.
     */
    importAll(folder, orbitDbUrl) {
        if (!fs.existsSync(folder)) {
            console.log("The folder ", folder, " doesn't exist.");
            return;
        }
        console.log('Importing all data contained in the folder ' + folder + ' into ' + orbitDbUrl);
        var dir = fs.readdirSync(folder);
        let specFiles = dir.filter(function (elm) { return elm.match(".*" + SCHEMA_FILE_SUFFIX); });
        let dataFiles = dir.filter(function (elm) { return elm.match(".*" + DATA_FILE_SUFFIX); });
        console.log("Found schema files: " + specFiles);
        console.log("Found data files: " + dataFiles);
        var numberOfSpecs = specFiles.length;
        var numberOfDatas = dataFiles.length;
        var i = 0;
        var d = 0;
        var processSchemaFunc = () => {
            this.importSchema(folder + "/" + specFiles[i], orbitDbUrl, (res) => {
                i++; 
                if (i < numberOfSpecs) {
                    setTimeout(processSchemaFunc, 100);
                }else{
                    if (d < numberOfDatas) {
                        setTimeout(processDataFunc, 100);
                    }
                }
            });
        }
        if (i < numberOfSpecs) {
            setTimeout(processSchemaFunc, 100);
        }

        var processDataFunc = () => {
            this.importData(folder + "/" + dataFiles[d], orbitDbUrl, (res) => {
                d++; 
                if (d < numberOfDatas) {
                    setTimeout(processDataFunc, 100);
                }
            });
        }
    }



    /**
     * It dumps all the DB specs and data to a given folder.
     * @param {*} orbitDbUrl 
     * @param {*} folder 
     */
    dumpAll(orbitDbUrl, folder) {
        //get dbs 
        this.getOrbitDbs(orbitDbUrl, (res) => {
            //console.log("Obtained: "+res)
            //create the destination folder
            if (!fs.existsSync(folder)) {
                fs.mkdirSync(folder, '0744');
            }
            let returnedDbs = JSON.parse(res);
            console.log("Found Dbs:")
            returnedDbs.forEach(
                (elem) => {
                    console.log("* " + elem.dbname);
                    this.dumpSchema(orbitDbUrl + "/db/" + elem.dbname + "", folder + "/" + elem.dbname + SCHEMA_FILE_SUFFIX);
                    this.dumpData(orbitDbUrl + "/db/" + elem.dbname + "", folder + "/" + elem.dbname + DATA_FILE_SUFFIX);
                }
            )
        });
    }

    /** 
     * Get all dbs 
     * @param {*} orbitDbUrl 
     * @param {*} res optional function to be called when we have the result.
     * @returns 
     */
    getOrbitDbs(orbitDbUrl, res) {
        let orbitDbUrlGetAll = orbitDbUrl + ORBITDB_GET_DBS;
        console.log("Getting all the dbs existing at: " + orbitDbUrlGetAll);
        http.get(orbitDbUrlGetAll, (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
                data += chunk;
            })
            resp.on('close', () => {
                console.log(" OK.");
                res(data);
            })
        }).on('error', (err) => { console.log('There was an error dumping data from ' + orbitDbUrl + " err:" + err.message) });
    }

    /**
     * It imports the data contained in file into the orbitDbUrl. 
     * @param {*} file The data file containing the data.
     * @param {*} orbitDbUrl The orbitDb URL to importa data into.
     * @param {*} res2 Optional callback, it will be called when the processing has ended.
     */
    importData(file, orbitDbUrl, res2) {
        console.log('Importing data contained in ' + file + ' into ' + orbitDbUrl);
        if (!fs.existsSync(file)) {
            console.log("File " + file + " doesn't exists. Can't continue.");
            if (res2) {
                res2(false);
            }
            return;
        }
        console.log("Processing " + file + ":");
        var parallelRequests = 0;
        const pipeline = fs.createReadStream(file).pipe(StreamArray.withParser());
        pipeline.on('data', (data) => {

            parallelRequests++;
            if (parallelRequests > MAX_CONCURRENT_CONNECTIONS) {
                pipeline.pause();
            }
            this._post(orbitDbUrl, JSON.stringify(data.value), ORBITDB_CREATE_DOC_PATH, (res) => {
                parallelRequests--;
                if (parallelRequests < MAX_CONCURRENT_CONNECTIONS) {
                    pipeline.resume();
                }
                if (res) {
                    console.log("imported element:" + data.key);
                } else {
                    console.log("Error importing: " + data.key+" from "+file);
                }

            });
        });
        pipeline.on("finish", () => { if (res2) { res2(true); } });
        pipeline.on("error", () => { if (res2) { res2(false); } });
    }

    /**
     * It creates a given remote database given an existing schema.
     * @param {*} file The file containing the schema.
     * @param {*} orbitDbUrl The URL of the destination OrbitDb instance.
     */
    importSchema(file, orbitDbUrl, res2) {
        if (!fs.existsSync(file)) {
            console.log("File " + file + " doesn't exists. Can't continue.");
            res2(false);
            return;
        }
        console.log('Using schema contained in ' + file + ' to a database a in ' + orbitDbUrl);
        var spec = fs.readFileSync(file);
        if (spec) {
            //console.log("spec is " + spec);
            var dbSpec = JSON.parse(spec);
            var indexBy = dbSpec.options.indexBy;
            var dbname = dbSpec.dbname;
            //console.log("indexBy:" + indexBy);
            //console.log("dbname:" + dbname);
            this.createOrbitDatabase(orbitDbUrl, dbname, indexBy, res2);
        } else {
            console.log("Unparsable content:" + spec);
            res2(false);
        }
    }

    /**
     * Creates a given orbitdb database.
     * @param {*} orbitDbUrl The orbitDb base URL.
     * @param {*} dbname The database name.
     * @param {*} indexBy The indexed by orbitdb creation option.
     * @param {*} res Optional call back with the result of the operation.
     */
    createOrbitDatabase(orbitDbUrl, dbname, indexBy, res) {
        let postData = JSON.stringify(this.createDbPayload(indexBy));
        this._post(orbitDbUrl, postData, "/db/" + dbname, res);
    }

    /**
     * It creates a create database request.
     * @param {*} indexBy 
     */
    createDbPayload(indexBy) {
        return { "create": true, "type": "docstore", "accessController": { "write": ["*"] }, "indexBy": indexBy };
    }

    /**
     * It dumps all the data to the given file.
     * @param {*} orbitDbUrl 
     * @param {*} file 
     * @param {*} res Optional callback it will be called when finished.
     */
    dumpData(orbitDbUrl, file, res) {
        console.log('Dumping database from ' + orbitDbUrl + ' to file ' + file);
        this._get(orbitDbUrl, file, ORBITDB_GET_ALL_PATH, res);
    }

    /**
     * It dumps the database schema to the the given file.
     * @param {*} orbitDbUrl 
     * @param {*} file
     * @param {*} res Optional callback it will be called when finished.
     */
    dumpSchema(orbitDbUrl, file, res) {
        console.log('Dumping database spec from ' + orbitDbUrl + ' to file ' + file);
        this._get(orbitDbUrl, file, ORBITDB_GET_SCHEMA_PATH, res);
    }

    /**
     * Download the content from a given URL and write it to a given file.
     * @param {*} baseUrl The base URL to make the request.
     * @param {*} file  The file to store the content.
     * @param {*} pathToOperation The path that will be concatenated to the base URL.
     * @param {*} res Optional callback it will be called when finished.
     */
    _get(baseUrl, file, pathToOperation, res) {
        if (fs.existsSync(file)) {
            console.log("File " + file + " already exists. Refusing to rewrite file.");
            return;
        }
        var dumpFile = fs.createWriteStream(file, { flags: 'a' });

        let orbitDbUrlGetAll = baseUrl + pathToOperation;
        http.get(orbitDbUrlGetAll, (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
                data += chunk;
                dumpFile.write(chunk);
                process.stdout.write(".");
            })
            resp.on('close', () => {
                console.log("\nFile " + file + " OK.");
                dumpFile.close();
                if (data) {
                    if (res) {
                        res(true);
                    }
                } else {
                    if (res) {
                        res(false);
                    }
                }
            })
        }).on('error', (err) => { console.log('There was an error dumping data from ' + baseUrl + " err:" + err.message) });
    }

    /**
     * Make a simple HTTP POST.
     * @param {*} url The URL.
     * @param {*} data The data to be posted.
     * @param {*} pathToOperation The path that will be concatenated to the base URL.
     * @param {*} res The optional callback to be invoked at operation completion.
     */
    _post(url, data, pathToOperation, res2) {
        let finalUrl = new URL(url + pathToOperation);
        let options = {
            'method': 'POST',
            'headers': {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(data)
            }
        }
        //console.log("request options are: " + JSON.stringify(options));
        http.request(finalUrl, options, res => {
            let data = ""
            res.on("data", d => {
                data += d
            })
            res.on("end", () => {
                //console.log(data);
                if (res2) {
                    res2(true);
                }
            })
        })
            .on("error", (error) => {
                if (res2) {
                    res2(false);
                } else {
                    console.error(error);
                }
            }).end(data);
    }

}

function usage() {
    console.log("Usage: ./orbitmigrator.js [OPTIONS]... [DB URL]...  [ORBIT ROOT URL]... [FILE]...");
    console.log("");
    console.log("Description: It import or exports database data and schemas between the file system and a given OrbitDb instance.");
    console.log("Each desired operation is identified by a given option followed its needed parameters.");
    console.log("It can process data and schema dumps and imports in batch.");
    console.log("")
    console.log("Options:");
    console.log("\t" + OPTION_CMD_DUMP_DATA + "\tIt dumps all the data of the database given by [DB URL] to [FILE].");
    console.log("\t" + OPTION_CMD_DUMP_SCHEMA + "\tIt dump the schema of the database given by [DB URL] to [FILE].");
    console.log("\t" + OPTION_CMD_IMPORT_DATA + "\tIt imports the JSON data contained in the dump [FILE] with all the elements to the destination [DB URL].");
    console.log("\t" + OPTION_CMD_IMPORT_SCHEMA + "\tIt imports a given schema in [FILE] with the database definition to a remote database [DB URL].");
    console.log("\t" + OPTION_CMD_DUMP_ALL + "\tIt dumps all the existing databases in a given instance [ORBIT ROOT URL] to a given folder [FOLDER].")
    console.log("\t" + OPTION_CMD_IMPORT_ALL + "\tIt imports all the existing data (please note that the files must respect the same format of the " + OPTION_CMD_DUMP_ALL + " option) into the OrbitDb instance given by [ORBIT ROOT URL].")
    console.log("");
    console.log("Parameters:");
    console.log("\t[FILE]"+"\t\t\tA given file existing in the file system containing data or a schema.");
    console.log("\t[FOLDER]"+"\t\tA given folder existing in the file system containing data or a schema files, previously dumped.");
    console.log("\t[DB URL]"+"\t\tThe OrbitDb URL for a given db instance. Please note that each OrbitDb db has a different URL.");
    console.log("\t[ORBIT ROOT URL]"+"\tThe OrbitDb root URL. This will be the root for the OrbitDb instance, without specifying a given database.");
}

let args = process.argv.slice(2);
if (args.length < 1) {
    usage();
    process.exit(-1);
}

let cmd = args[0];

if (cmd === OPTION_CMD_HELP) {
    usage();
    process.exit(0);
} else if (cmd === OPTION_CMD_DUMP_DATA) {
    let orbitDbMigrator = new OrbitMigrator();
    if (args.length < 3) {
        console.log(OPTION_CMD_DUMP_DATA + " is followed by the database [DB URL] and the [FILE] to dump the data.")
        process.exit(-1);
    }
    orbitDbMigrator.dumpData(args[1], args[2]);
} else if (cmd === OPTION_CMD_DUMP_SCHEMA) {
    let orbitDbMigrator = new OrbitMigrator();
    if (args.length < 3) {
        console.log(OPTION_CMD_DUMP_SCHEMA + " is followed by the database [DB URL] and the [FILE] to dump the schema.")
        process.exit(-1);
    }
    orbitDbMigrator.dumpSchema(args[1], args[2]);
} else if (cmd === OPTION_CMD_IMPORT_DATA) {
    let orbitDbMigrator = new OrbitMigrator();
    if (args.length < 3) {
        console.log(OPTION_CMD_IMPORT_DATA + " is followed by the [FILE] with the data to import and the [DB URL] to import into.")
        process.exit(-1);
    }
    orbitDbMigrator.importData(args[1], args[2]);
} else if (cmd === OPTION_CMD_IMPORT_SCHEMA) {
    let orbitDbMigrator = new OrbitMigrator();
    if (args.length < 3) {
        console.log(OPTION_CMD_IMPORT_SCHEMA + " is followed by the [FILE] with the schema to create and the orbit instance [ORBIT URL] to store into.")
        process.exit(-1);
    }
    orbitDbMigrator.importSchema(args[1], args[2]);
} else if (cmd === OPTION_CMD_DUMP_ALL) {
    let orbitDbMigrator = new OrbitMigrator();
    if (args.length < 3) {
        console.log(OPTION_CMD_DUMP_ALL + " is followed by the [ORBIT URL] with the data to import and the [FOLDER] to store into.")
        process.exit(-1);
    }
    orbitDbMigrator.dumpAll(args[1], args[2]);
} else if (cmd === OPTION_CMD_IMPORT_ALL) {
    let orbitDbMigrator = new OrbitMigrator();
    if (args.length < 3) {
        console.log(OPTION_CMD_IMPORT_ALL + " is followed by the [FOLDER] and the [ORBIT URL] to import into.")
        process.exit(-1);
    }
    orbitDbMigrator.importAll(args[1], args[2]);
} else {
    usage();
}
